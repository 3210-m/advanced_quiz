package com.twuc.webApp.service;


import com.twuc.webApp.entity.Orders;
import com.twuc.webApp.entity.Product;
import com.twuc.webApp.repository.ProductRepository;
import com.twuc.webApp.service.ProductService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
@ActiveProfiles("test")
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD)
class SimpleServiceTest {

    @Autowired
    private ProductService productService;

    @Autowired
    private OrdersService ordersService;

    @Test
    void name() {
    }

    @Test
    void should_create_product_and_return_it_with_id() {
        Product product = new Product("可乐", 2.5, "瓶", "https://ss0.bdstatic.com/70cFuHSh_Q1YnxGkpoWK1HF6hhy/it/u=2227910659,4202652301&fm=26&gp=0.jpg");
        Product product1 = productService.createProduct(product);

        assertEquals(1L,product1.getId().longValue());
    }

    @Test
    void should_get_all_orders() {
        List<Orders> allOrders = ordersService.getAllOrders();
        assertEquals(1,allOrders.size());
    }
}

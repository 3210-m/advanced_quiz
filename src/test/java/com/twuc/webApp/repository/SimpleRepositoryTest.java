package com.twuc.webApp.repository;


import com.twuc.webApp.entity.Orders;
import com.twuc.webApp.entity.Product;
import com.twuc.webApp.repository.OrdersRepository;
import com.twuc.webApp.repository.ProductRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;

import javax.persistence.EntityManager;
import java.util.Date;

import static org.junit.jupiter.api.Assertions.assertEquals;

@ActiveProfiles("test")
@DataJpaTest(showSql = false)
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD)
public class SimpleRepositoryTest {
    
    @Autowired
    private ProductRepository productRepository;
    
    @Autowired
    private OrdersRepository ordersRepository;
    
    @Autowired
    private EntityManager entityManager;


    @Test
    void should_create_recards_to_two_table() {
        Product product = new Product("可乐",2.5 ,"瓶","https://ss0.bdstatic.com/70cFuHSh_Q1YnxGkpoWK1HF6hhy/it/u=2227910659,4202652301&fm=26&gp=0.jpg");
        productRepository.saveAndFlush(product);
        entityManager.clear();

        Orders orders = new Orders(1,1,new Date(), null, product);

        Orders orders1 = ordersRepository.saveAndFlush(orders);

        assertEquals(1L,product.getId().longValue());
        assertEquals(1L,orders.getOrderId().longValue());

    }
}

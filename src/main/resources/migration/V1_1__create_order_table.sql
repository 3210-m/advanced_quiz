create table if not exists orders(
    order_id bigInt primary key AUTO_INCREMENT,
    product_id bigInt not null,
    product_number int not null,
    status int not null,
    createTime date not null,
    foreign key(product_id) references product(id)
)

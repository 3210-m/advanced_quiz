create table if not exists product(
    id bigInt primary key AUTO_INCREMENT,
    product_name varchar(25) not null ,
    price double not null ,
    unit int not null,
    img varchar(255) not null
)
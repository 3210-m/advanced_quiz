package com.twuc.webApp.web;

import com.twuc.webApp.entity.Orders;
import com.twuc.webApp.entity.Product;
import com.twuc.webApp.service.OrdersService;
import com.twuc.webApp.service.ProductService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api")
@CrossOrigin(origins = "*")
public class PostController {
    private final ProductService productService;
    private final OrdersService ordersService;

    public PostController(ProductService productService, OrdersService ordersService) {
        this.productService = productService;
        this.ordersService = ordersService;
    }

    @PostMapping("/addproduct")
    public ResponseEntity createProduct(@RequestBody Product product){
        Product expectProduct = productService.createProduct(product);
        return ResponseEntity.status(HttpStatus.CREATED).body(expectProduct);

    }


    /**
     * two operations:create orders foreign productId
     * @param orders
     * @return
     */
    @PostMapping("/addproductToorderslist")
    public ResponseEntity addProductToOrdersList(@RequestBody Orders orders){
        Orders expectOrders = ordersService.createOrUpDateOrders(orders);
        return ResponseEntity.status(HttpStatus.CREATED).body(expectOrders);
    }

    @GetMapping("/getallorders")
    public ResponseEntity getAllOrders(){
        List<Orders> allOrders = ordersService.getAllOrders();
        return ResponseEntity.status(HttpStatus.OK).body(allOrders);
    }
    @GetMapping("/getallproducts")
    public ResponseEntity getAllProducts(){
        List<Product> allProducts = productService.getAllProducts();
        return ResponseEntity.status(HttpStatus.OK).body(allProducts);
    }

    @PostMapping("/deleteorder")
    public ResponseEntity deleteOrder(@RequestBody Orders order){
        Long deleteLine = ordersService.deleteOrder(order);
        return ResponseEntity.status(HttpStatus.OK).body(deleteLine);
    }


}

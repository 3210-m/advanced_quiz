package com.twuc.webApp.entity;

import javax.persistence.*;

@Entity
public class Product {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false)
    private String productName;

    @Column(nullable = false)
    private Double price;

    @Column(nullable = false)
    private String unit;

    @Column(nullable = false)
    private String img;

    public Product() {
    }

    public Product(String productName, Double price, String unit, String img) {
        this.productName = productName;
        this.price = price;
        this.unit = unit;
        this.img = img;
    }

    public Long getId() {
        return id;
    }

    public String getProductName() {
        return productName;
    }

    public Double getPrice() {
        return price;
    }

    public String getUnit() {
        return unit;
    }

    public String getImg() {
        return img;
    }
}

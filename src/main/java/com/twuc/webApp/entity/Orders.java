package com.twuc.webApp.entity;

import javax.persistence.*;
import java.util.Date;

@Entity(name = "orders")
public class Orders {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long orderId;

    @Column(nullable = false)
    private int product_number = 1 ;

    //状态默认成功 1
    @Column(nullable = false)
    private int status = 1;


    @Column(nullable = false)
    private Date createTime = new Date();

    @Column
    private Date updateTime;

    @ManyToOne
    @JoinColumn(name = "product_id")
    private Product productId;

    public Orders() {
    }

    public Orders(int product_number, int status, Date createTime, Date updateTime, Product productId) {
        this.product_number = product_number;
        this.status = status;
        this.createTime = createTime;
        this.updateTime = updateTime;
        this.productId = productId;
    }


    public void setOrderId(Long orderId) {
        this.orderId = orderId;
    }

    public void setProduct_number(int product_number) {
        this.product_number = product_number;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public Long getOrderId() {
        return orderId;
    }

    public int getProduct_number() {
        return product_number;
    }

    public int getStatus() {
        return status;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public Product getProductId() {
        return productId;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

}

package com.twuc.webApp.repository;

import com.twuc.webApp.entity.Orders;
import com.twuc.webApp.entity.Product;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface OrdersRepository extends JpaRepository<Orders,Long> {
    Optional<Orders> findByProductId(Product productId);

    Long deleteByProductId(Product product);
}

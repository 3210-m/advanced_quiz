package com.twuc.webApp.service;

import com.twuc.webApp.entity.Orders;

import java.util.List;

public interface OrdersService {
    Orders createOrUpDateOrders(Orders orders);
    List<Orders> getAllOrders();

    Long deleteOrder(Orders order);
}

package com.twuc.webApp.service;

import com.twuc.webApp.entity.Product;

import java.util.List;

public interface ProductService {
    Product createProduct(Product product);
    List<Product> getAllProducts();
}

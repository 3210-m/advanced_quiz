package com.twuc.webApp.service.impl;

import com.twuc.webApp.entity.Product;
import com.twuc.webApp.repository.ProductRepository;
import com.twuc.webApp.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import java.util.List;

@Service
public class ProductServiceImpl implements ProductService {

    @Autowired
    private ProductRepository productRepository;

    @Autowired
    private EntityManager entityManager;

    @Override
    public Product createProduct(Product product) {
        productRepository.saveAndFlush(product);
        entityManager.clear();
        return product;
    }

    @Override
    public List<Product> getAllProducts() {
        List<Product> allProducts = productRepository.findAll();
        return allProducts;
    }
}

package com.twuc.webApp.service.impl;

import com.twuc.webApp.entity.Orders;
import com.twuc.webApp.repository.OrdersRepository;
import com.twuc.webApp.service.OrdersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
public class OrdersServiceImpl implements OrdersService {

    @Autowired
    private OrdersRepository ordersRepository;

    @Autowired
    private EntityManager entityManager;

    @Override
    public Orders createOrUpDateOrders(Orders orders) {
        Optional<Orders> byProductId = ordersRepository.findByProductId(orders.getProductId());
        if(byProductId.isPresent()){
            orders.setOrderId(byProductId.get().getOrderId());
            orders.setProduct_number(byProductId.get().getProduct_number()+1);
            orders.setUpdateTime(new Date());
        }
        ordersRepository.saveAndFlush(orders);
        entityManager.clear();
        return orders;
    }

    @Override
    public List<Orders> getAllOrders() {
        return ordersRepository.findAll();
    }

    @Override
    @Transactional
    public Long deleteOrder(Orders order) {
        return ordersRepository.deleteByProductId(order.getProductId());
    }
}
